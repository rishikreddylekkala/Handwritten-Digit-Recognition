# Handwritten-Digit-Recognition
Handwritten Digit Recognition using Transfer Learning and MNIST Dataset

Finetuned LeNet-5 Architecture Model and it is optimized to work with greyscale images and can accurately identify single digits. The model is deployed using Flask Flamework, which allows for easy integration into web applications.

[Implementation of LeNet-5 Architecture](https://github.com/RISHIKREDDYL/LeNet-5)

# References
LeCun, Y., Bottou, L., Bengio, Y., & Haffner, P. (1998). [Gradient-based learning applied to document recognition.](https://hal.science/hal-03926082/document) Proceedings of the IEEE, 86(11), 2278-2324.

![image](https://github.com/RISHIKREDDYL/Handwritten-Digit-Recognition/assets/36101925/d847fa01-dcb2-4917-9562-a90f55724090)
